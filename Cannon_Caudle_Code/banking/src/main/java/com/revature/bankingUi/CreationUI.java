package com.revature.bankingUi;

import java.util.Scanner;

import com.revature.bank.service.BankingService;
import com.revature.bankingApp.InfiniteUI;
import com.revature.bankingBeans.AccountBean;
import com.revature.bankingBeans.UserBean;

public class CreationUI {
	public void creationUi(Scanner reader, UserBean user) {
		
		
		
		int userChoice;
		boolean quit = false;

		System.out.println("----------------------------");
		System.out.println("--						  --");
		System.out.println("--      SpaceBanker       --");
		System.out.println("--                        --");
		System.out.println("----------------------------");

		do {
			System.out.print("For creating a Checking type 1 \n");
			System.out.print("For Savings a Account type 2 \n");
			System.out.print("or type 0 to exit \n ");
			// switch case for checking and savings
			
			userChoice = reader.nextInt();
			switch (userChoice) {

			case 1:
				createChecking(reader, user);
				
				break;
			case 2:
				createSaving(reader, user);
				break;
			case 0:
				InfiniteUI.main(null);
				break;

			default:
				System.out.println("please try again");
				break;
			}
		} while (!quit);

	}	
	
	public void createChecking(Scanner reader, UserBean user) {
		
		BankingService bankService = new BankingService();
		
		AccountBean account = new AccountBean();
		String acctsName;
		
		System.out.println("----------------------------");
		System.out.println("--						  --");
		System.out.println("--      SpaceBanker       --");
		System.out.println("--                        --");
		System.out.println("----------------------------");
		
		
		reader.nextLine();
		
		System.out.print("Please enter the Checkings accounts name: \n");
		
		acctsName = reader.nextLine();
		account.setAcctsName(acctsName);
		int acctsType = 1;
		
		account.setAcctsType(1);
		
		
		user.getUserid();
		bankService.createAccount(acctsName, acctsType, user);
		
	}
	
	public void createSaving(Scanner reader, UserBean user) {
		
		BankingService bankService = new BankingService();
		AccountBean account = new AccountBean();
		String acctsName;
		
		System.out.println("----------------------------");
		System.out.println("--						  --");
		System.out.println("--      SpaceBanker       --");
		System.out.println("--                        --");
		System.out.println("----------------------------");
		
		reader.nextLine();
		System.out.print("Please enter the Savings accounts name: \n");
		
		acctsName = reader.nextLine();
		account.setAcctsName(acctsName);

		int acctsType = 2;
		
		account.setAcctsType(2);
		
		bankService.createAccount(acctsName, acctsType, user);
		
		
	}

	
}
