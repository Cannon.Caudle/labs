package com.revature.bankingUi;

import java.util.Scanner;

import com.revature.bank.service.BankingService;
import com.revature.bankingBeans.AccountBean;
import com.revature.bankingBeans.UserBean;

public class InteractionUI {
	public void interactiveUi(Scanner reader, UserBean user, AccountBean account) {

		AccInterface accInterface = new AccInterface();
		BankingService bankService = new BankingService();
		System.out.println("----------------------------");
		System.out.println("--						  --");
		System.out.println("--      SpaceBanker       --");
		System.out.println("--                        --");
		System.out.println("----------------------------");

		// get list of bank_accounts
		// get account
		
		int userChoice;
		boolean quit = false;
		System.out.print("Check Balance type 1 \n");
		System.out.print("Withdraw type 2 \n");
		System.out.print("Deposit type 3 \n ");
		System.out.print("Transfer funds type 4 \n");
		System.out.println("view Account details 5 \n");
		System.out.println("join another account 6 \n");
		System.out.println("type 0 to quit \n");
		
		userChoice = reader.nextInt();
		do {
			switch (userChoice) {
			case 1:
				System.out.println("type in the account number");
				account.setAcctsID(reader.nextInt());
				bankService.balanceCheck(account);
				interactiveUi(reader, user, account);
				break;
			case 2:
				System.out.println("type in the account number");
				account.setAcctsID(reader.nextInt());
				System.out.println("how much would you like to withdraw ?");
				int amount = reader.nextInt();
				bankService.withdraw(account, amount);
				interactiveUi(reader, user, account);
				break;
			case 3:
				System.out.println("type in the account number");
				account.setAcctsID(reader.nextInt());
				System.out.println("how much would you like to deposit ?");
				int amountB = reader.nextInt();
				
				bankService.deposit(account, amountB);
				interactiveUi(reader, user, account);
				break;
			case 4:
				System.out.println("type in the account number you wish to take from");
				account.setAcctsID(reader.nextInt());
				System.out.println("how much would you like to transfer?");
				int amountC = reader.nextInt();
				bankService.withdraw(account, amountC);
				
				System.out.println("type in the account number you wish to deposit");
				account.setAcctsID(reader.nextInt());
				bankService.deposit(account, amountC);
				interactiveUi(reader, user, account);

				break;
			case 5:
				user.getUserid();
				bankService.userAccounts(user, account);
				//System.out.println(account.getAcctsID());
				interactiveUi(reader, user, account);
				break;
				
			case 6:
				user.getUserid();
				
				System.out.println("please enter the Checkings or Savings Account Number: ");
				int accountNumb = reader.nextInt();
				System.out.println("Thanks");
				//reader.nextLine();
				
				System.out.println("please enter the userID of the account: ");
				int userID = reader.nextInt();
				reader.nextLine();
				
				System.out.println("please enter the Name of the User's Account: ");
				String accountN = reader.nextLine();
				
				System.out.println("Thanks");
				System.out.println("please enter the password of the User's Account: ");
				String passWord = reader.nextLine();
				
				bankService.accountJoin(account, user, accountNumb, userID, accountN, passWord);
				interactiveUi(reader, user, account);
				break;
				
			case 0:
				quit = true;
				break;
			default:
				System.out.println("please try again");
				break;
			}
		} while (!quit);
		accInterface.accountUi(reader, user);
	}
}
