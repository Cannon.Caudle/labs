package com.revature.bankingUi;

import java.util.Scanner;

import com.revature.bankingApp.InfiniteUI;
import com.revature.bankingBeans.AccountBean;
import com.revature.bankingBeans.UserBean;

public class AccInterface {

	public void accountUi(Scanner reader, UserBean user) {
		AccountBean account = new AccountBean();
		CreationUI creation = new CreationUI();
		InteractionUI interact = new InteractionUI();
		int userChoice;
		boolean quit = false;

		System.out.println("----------------------------");
		System.out.println("--						  --");
		System.out.println("--      SpaceBanker       --");
		System.out.println("--                        --");
		System.out.println("----------------------------");

		do {
			System.out.print("For creating a Checking or Savings Account type 1 \n");
			System.out.print("to view or interact with your Checkings or Savings Account type 2 \n");
			System.out.print("or type 0 to Log out \n ");

			userChoice = reader.nextInt();
			switch (userChoice) {

			case 1:
				creation.creationUi(reader, user);
				break;
			case 2:
				interact.interactiveUi(reader, user, account);
				break;

			case 0:
				
				InfiniteUI.main(null);
				break;

			default:
				System.out.println("please try again");
				break;
			}
		} while (!quit);

	}

}
