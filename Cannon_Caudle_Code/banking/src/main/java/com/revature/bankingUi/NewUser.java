package com.revature.bankingUi;

import java.util.Scanner;

import com.revature.bank.service.BankingService;
import com.revature.bankingBeans.UserBean;

public class NewUser {
	public void menuNewUser(Scanner reader) {
		String fname, lname, acctname, password;
		AccInterface acctIface = new AccInterface();
		UserBean user = new UserBean();
		BankingService bankService = new BankingService();

		System.out.println("----------------------------");
		System.out.println("--						  --");
		System.out.println("--    Welcome New User    --");
		System.out.println("--           TO           --");
		System.out.println("--       SPACEBANKER      --");
		System.out.println("--                        --");
		System.out.println("----------------------------");
		reader.nextLine();
		
		System.out.print("Enter your first name: ");
		fname = reader.nextLine();
		user.setFname(fname);


		System.out.print("Enter your last name: ");
		lname = reader.nextLine();
		user.setLname(lname);
		
		do {
		System.out.print("Please enter your new Account name: ");
		acctname = reader.nextLine();
		user.setAcctname(acctname);
		}while(bankService.checkUserName(acctname) == true);
		
		System.out.print("Pleaser enter your Password: ");
		password = reader.nextLine();
		user.setPassword(password);

		bankService.createUserAccount(fname, lname, acctname, password);
		
		bankService.getUserByAcctname(acctname, password, user);
		System.out.println("your account ID is: " + user.getUserid());
		acctIface.accountUi(reader, user);
		
		// auto generate userID
	}
}