package com.revature.bankingApp;

import java.util.Scanner;

import com.revature.bankingUi.NewUser;
import com.revature.bankingUi.ReturningUser;

public class InfiniteUI {

	public static void main(String[] args) {

		
		NewUser menu = new NewUser();
		ReturningUser rMenu = new ReturningUser();
		final Scanner reader = new Scanner(System.in);
		

		int userChoice;
		boolean quit = false;
		System.out.println("----------------------------");
		System.out.println("--						  --");
		System.out.println("-- Welcome to SpaceBanker --");
		System.out.println("--                        --");
		System.out.println("----------------------------");

		do {
			System.out.print("For new users type 1 \n");
			System.out.print("For returning customers type 2 \n");
			System.out.print("or type 0 to quit \n ");
			

			userChoice = reader.nextInt();
			switch (userChoice) {
			case 1:
				menu.menuNewUser(reader);
				break;
			case 2:
				rMenu.ReturningMenu(reader);
				break;
			case 0:
				reader.close();
				quit = true;
				System.exit(0);
				break;

			default: System.out.println("please try again");
			break;
			}
		}while (!quit);
		
	}


}
