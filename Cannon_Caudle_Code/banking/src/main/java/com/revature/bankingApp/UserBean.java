/**
 * 
 */
package com.revature.bankingApp;

/**
 * @author cannon
 *
 */
public class UserBean {

	private long userid;
	private String fname;
	private String lname;
	private String acctname;
	private String password;
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getAcctname() {
		return acctname;
	}
	public void setAcctname(String acctname) {
		this.acctname = acctname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public UserBean(long userid, String fname, String lname, String acctname, String password) {
		super();
		this.userid = userid;
		this.fname = fname;
		this.lname = lname;
		this.acctname = acctname;
		this.password = password;
	}
	
}
