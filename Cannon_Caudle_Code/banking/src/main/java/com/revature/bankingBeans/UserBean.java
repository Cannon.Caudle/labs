/**
 * 
 */
package com.revature.bankingBeans;

/**
 * @author cannon
 *
 */
public class UserBean {

	private int userid;
	private String fname;
	private String lname;
	private String acctname;
	private String password;
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getAcctname() {
		return acctname;
	}
	public void setAcctname(String acctname) {
		this.acctname = acctname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public UserBean() {
		super();
	}
	public UserBean(int userid, String fname, String lname, String acctname, String password) {
		super();
		this.userid = userid;
		this.fname = fname;
		this.lname = lname;
		this.acctname = acctname;
		this.password = password;
	}
	
	
}
