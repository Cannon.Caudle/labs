package com.revature.bankingBeans;

public class AccountBean {
	private int acctsID;
	private int acctsType;
	private String acctsName;
	private int balance;
	
	public int getAcctsID() {
		return acctsID;
	}
	public void setAcctsID(int acctsID) {
		this.acctsID = acctsID;
	}
	public int getAcctsType() {
		return acctsType;
	}
	public void setAcctsType(int acctsType) {
		this.acctsType = acctsType;
	}
	public String getAcctsName() {
		return acctsName;
	}
	public void setAcctsName(String acctsName) {
		this.acctsName = acctsName;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	public AccountBean(int acctsID, int acctsType, String acctsName, int balance) {
		super();
		this.acctsID = acctsID;
		this.acctsType = acctsType;
		this.acctsName = acctsName;
		this.balance = balance;
	}
	public AccountBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}

