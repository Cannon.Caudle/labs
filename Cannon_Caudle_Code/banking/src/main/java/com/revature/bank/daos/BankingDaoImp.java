package com.revature.bank.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.revature.bank.connectionUtil.ConnectionUtil;
import com.revature.bankingApp.InfiniteUI;
import com.revature.bankingBeans.AccountBean;
import com.revature.bankingBeans.UserBean;
import com.revature.bankingUi.InteractionUI;
import com.revature.bankingUi.NewUser;

public class BankingDaoImp implements BankingDao {

	NewUser newTry = new NewUser();
	InfiniteUI track = new InfiniteUI();
	InteractionUI ui = new InteractionUI();
	private ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();

	public UserBean getUserByAcctname(String acctname, String password, UserBean user) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT * FROM bank_user WHERE acct_name=? and password=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, acctname);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {

				user.setUserid(rs.getInt("userid"));
				
			} else {
				// not found
				System.out.println("we didnt find that account");
			}
			rs.close();
			ps.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public AccountBean getBankByuserId(UserBean user, AccountBean account) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT accts_type, accts_name, accts_balance FROM bank_account WHERE accts_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, account.getAcctsID());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				account.setAcctsName(rs.getString("accts_name"));
				account.setAcctsName(rs.getString("accts_type"));
				account.setBalance(rs.getInt("accts_balance"));
				System.out.println(account.getAcctsID());
				System.out.println(account.getAcctsName());
				System.out.println(account.getBalance());
				return account;
			} else {

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return account;
	}

	public UserBean saveUser(UserBean user) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO bank_user (fname, lname, acct_name, password) VALUES ( ?, ?, ?, ?) RETURNING userid";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getFname());
			ps.setString(2, user.getLname());
			ps.setString(3, user.getAcctname());
			ps.setString(4, user.getPassword());

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				
				user.setUserid(rs.getInt("userid"));
			
			}
			rs.close();
			ps.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	public boolean checkUserName(String acctname) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT * FROM bank_user where acct_name=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, acctname);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				// found
				System.out.println("Sorry that name already exists");
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public AccountBean saveAccount(AccountBean account, UserBean user) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO bank_account (accts_type, accts_name) VALUES ( ?, ?) RETURNING accts_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, account.getAcctsType());
			ps.setString(2, account.getAcctsName());

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				account.setAcctsID(rs.getInt("accts_id"));
				account.getAcctsID();
				user.getUserid();
				saveComposite(user, account);
				System.out.println("your account number is: " + account.getAcctsID() +"\n");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return account;
	}

	public AccountBean saveComposite(UserBean user, AccountBean account) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO composite_table (userid, acctid) VALUES ( ?, ?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, user.getUserid());
			ps.setInt(2, account.getAcctsID());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return account;
	}

	public AccountBean userAccounts(UserBean user, AccountBean account) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT * FROM useraccess WHERE userid=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, user.getUserid());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println("Account Name: " + rs.getString("accts_name") + " Account ID: " + rs.getInt("acctid")
						+ " Account Balance: " + rs.getInt("accts_balance"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return account;
	}

	public AccountBean balanceCheck(AccountBean account) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT accts_balance FROM bank_account WHERE accts_id =?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, account.getAcctsID());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				account.setBalance(rs.getInt("accts_balance"));
				System.out.println("Your account balance is now : $" + account.getBalance());

			} else {
				// not found
				System.out.println("we didnt find that account sorry");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return account;
	}

	public AccountBean withdraw(AccountBean account, int amount) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql2 = "SELECT accts_balance FROM bank_account WHERE accts_id =?";
			PreparedStatement ps2 = conn.prepareStatement(sql2);
			ps2.setInt(1,account.getAcctsID());
			ResultSet rs = ps2.executeQuery();
			if (rs.next()) {
			account.setBalance(rs.getInt("accts_balance"));
			}
			String sql = "UPDATE bank_account SET accts_balance =? WHERE accts_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			
			int result =  account.getBalance() - amount;
			ps.setInt(1, result);
			ps.setInt(2, account.getAcctsID());
			ps.executeUpdate();
			account.setBalance(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return account;
	}

	public AccountBean deposit(AccountBean account, int amount) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql2 = "SELECT accts_balance FROM bank_account WHERE accts_id =?";
			PreparedStatement ps2 = conn.prepareStatement(sql2);
			ps2.setInt(1,account.getAcctsID());
			ResultSet rs = ps2.executeQuery();
			if (rs.next()) {
			account.setBalance(rs.getInt("accts_balance"));
			}
			String sql = "UPDATE bank_account SET accts_balance =? WHERE accts_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			
			int result =  account.getBalance() + amount;
			ps.setInt(1, result);
			ps.setInt(2, account.getAcctsID());
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return account;
	}
	
	
	public UserBean accountJoin(AccountBean account, UserBean user, int accountNumb, int userID, String accountN, String passWord) {
		try (Connection conn = connectionUtil.getConnection()) {
		String sql = "SELECT * FROM account_access WHERE acctid=?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, accountNumb);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			if((userID == rs.getInt("userid")) && ( accountN.equals(rs.getString("acct_name"))) && (passWord.equals(rs.getString("password")))) {
				System.out.println("You now has access to account number " + accountNumb);
				account.setAcctsID(accountNumb);
				
				saveComposite(user, account);
			}
		}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	

}

/*
 * public List<String> getMatchAccounts(UserBean user, AccountBean account){
 * List<String> accountList = new ArrayList(); try(Connection conn =
 * connectionUtil.getConnection()){ String sql =
 * "SELECT * FROM bank_account WHERE composite_table acctid = bank_account accts_id AND composite_table userid = bank_user userid=?"
 * ; PreparedStatement ps = conn.prepareStatement(sql); ps.setInt(1,
 * user.getUserid()); ResultSet rs = ps.executeQuery(); while(rs.next()) {
 * accountList.add(rs.getString("accts_id"));
 * accountList.add(rs.getString("accts_type")); } }catch (SQLException e) {
 * e.printStackTrace(); } return accountList; }
 */
