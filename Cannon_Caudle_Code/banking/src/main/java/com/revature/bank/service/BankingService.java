package com.revature.bank.service;


import com.revature.bank.daos.BankingDao;
import com.revature.bank.daos.BankingDaoImp;
import com.revature.bankingBeans.AccountBean;
import com.revature.bankingBeans.UserBean;

public class BankingService {
	
	private BankingDao dao = new BankingDaoImp();
	
/*	public UserBean saveUser(UserBean user) {
		return dao.saveUser(user);
	}*/
	
	public UserBean createUserAccount(String fname, String lname, String acctname, String password) {
		UserBean user = new UserBean();
		user.setFname(fname);
		user.setLname(lname);
		user.setAcctname(acctname);
		user.setPassword(password);
		dao.saveUser(user);
		return user;
		}
	
	public UserBean getUserByAcctname(String acctname, String password, UserBean user) {
		return dao.getUserByAcctname(acctname, password, user);
	}
	public boolean checkUserName(String acctname) {
		return dao.checkUserName(acctname);
	}
	
	public AccountBean createAccount(String acctsName, int acctsType, UserBean user) {
		AccountBean account = new AccountBean();
		account.setAcctsType(acctsType);
		account.setAcctsName(acctsName);
		user.getUserid();
		//System.out.println("createAccount");
		//System.out.println(user.getUserid());
		//System.out.println("createAccount");
		dao.saveAccount(account, user);
		return account;
	}
	
	public AccountBean getBankByuserId(UserBean user, AccountBean account) {
		return dao.getBankByuserId(user, account);
	}
	
	public AccountBean deposit(AccountBean account, int amount) {
		 dao.deposit(account, amount);
		return account;
	}
	
	public AccountBean withdraw(AccountBean account, int amount) {
		 dao.withdraw(account, amount);
		 return account;
	}
	
	public AccountBean balanceCheck(AccountBean account) {
		return dao.balanceCheck(account);
	}
	
	public AccountBean saveComposite(UserBean user, AccountBean account) {
		return dao.saveComposite(user, account);
	}
	
	public AccountBean userAccounts(UserBean user, AccountBean account) {
		return dao.userAccounts(user, account);
	}
	
	public UserBean accountJoin(AccountBean account, UserBean user, int accountNumb, int userID, String accountN, String passWord) {
		return dao.accountJoin(account, user, accountNumb, userID, accountN, passWord);
	}
	
	/*public List<String> getAccounts(UserBean user, AccountBean account){
		
		return dao.getMatchAccounts(user, account);
	}*/
	
	
}
