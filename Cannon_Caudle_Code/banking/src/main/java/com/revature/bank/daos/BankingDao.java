package com.revature.bank.daos;

import com.revature.bankingBeans.AccountBean;
import com.revature.bankingBeans.UserBean;

public interface BankingDao {
	
	
			
	public UserBean getUserByAcctname(String acctname, String password, UserBean user);
	public UserBean saveUser(UserBean user);
	public boolean checkUserName(String acctname);
	public AccountBean saveAccount(AccountBean account, UserBean user);
	
	public AccountBean deposit(AccountBean account, int amount);
	public AccountBean withdraw(AccountBean account, int amount);
	public AccountBean balanceCheck(AccountBean account);
	public AccountBean getBankByuserId(UserBean user, AccountBean account);
	public AccountBean saveComposite(UserBean user, AccountBean account);
	public AccountBean userAccounts(UserBean user, AccountBean account);
	public UserBean accountJoin(AccountBean account, UserBean user, int accountNumb, int userID, String accountN, String passWord);
	/*public List<String> getMatchAccounts(UserBean user, AccountBean account);
*/	
}
