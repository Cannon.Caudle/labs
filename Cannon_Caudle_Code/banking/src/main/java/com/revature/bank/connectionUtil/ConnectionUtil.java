package com.revature.bank.connectionUtil;



import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * the goal is to serve as a singleton which provides objects to DAO classes
 * when needded
 * 
 * @author cannon
 *
 */
public class ConnectionUtil {
	private static ConnectionUtil connectionUtil = new ConnectionUtil();

	private ConnectionUtil() {
	}

	public static ConnectionUtil getConnectionUtil() {
		return connectionUtil;
	}

	public Connection getConnection() {
		Properties properties = new Properties();
		try {

			properties.load(new FileReader("src/main/resources/database.properties"));

			String urlEnvName = properties.getProperty("url");
			String userEnvName = properties.getProperty("user");
			String passwordEnvName = properties.getProperty("password");

			
			String url = System.getenv(urlEnvName);
			String user = System.getenv(userEnvName);
			String password = System.getenv(passwordEnvName);
			
			return DriverManager.getConnection(url, user, password);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
		/*
		 * try { return DriverManager.getConnection(
		 * "jdbc:postgresql://pgdatabase.cm5ehuekd8tf.us-east-2.rds.amazonaws.com/pgDatabase",
		 * "CannonC1", "5Wpkjn4015!"); } catch (SQLException e) { e.printStackTrace(); }
		 */

	}

}