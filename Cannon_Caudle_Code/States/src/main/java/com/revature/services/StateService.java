package com.revature.services;

import com.revature.beans.State;
import com.revature.daos.StateDao;
import com.revature.daos.StateDaoImpl;

/**
 * 
 * Service layer is meant to be the business logic of our application.
 * this is where our adding and stuff going into the database.
 * 
 * @author Luis
 *
 */
public class StateService {
	
	private StateDao dao = new StateDaoImpl();
	
	public State getState(int id) {
		return dao.getStateById(id);
	}

}
