package com.revature.daos;
/**
 * 
 * DAO - Data Access Object
 * The object used to communicate with database.
 * The Dao classes composes one dinstinct layer of our application.
 * 
 * @author Luis
 *
 */

import com.revature.beans.State;

public interface StateDao {
	public State getStateById(int id);

	public State getStateByAbbreviation(String abbreviation);

	public void saveState(State state);

	public void updateState(State state);

	public void deleteState(State state);

}
