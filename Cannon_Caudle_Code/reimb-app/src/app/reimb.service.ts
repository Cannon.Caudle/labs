import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import { Reimb } from './classes/reimb';
import { User } from './classes/user';

@Injectable()
export class ReimbService {
  private privReimbs: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  public reimb$: Observable<any> = this.privReimbs.asObservable();

  constructor(private httpClient: HttpClient) { }

  public getReimbs() {
    this.httpClient.get<Reimb>(environment.apiNpoint + '/ReimbsGrabber/')
      .subscribe((payload) => {
        this.privReimbs.next(payload);
        console.log(payload);
      });

  }
  public writeReimb(that: any, userData) {
    this.httpClient.post<Reimb>(environment.apiNpoint + '/WriteReimbServlet/', JSON.stringify(that))
    .subscribe( (payload) => {
    this.privReimbs.next(payload);
    });
  }
}
