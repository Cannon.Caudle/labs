import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import { User } from './classes/user';
import { PersistenceService } from 'angular-persistence';

@Injectable()
export class LoginService {
  private privUser: BehaviorSubject<any> = new  BehaviorSubject<any>([]);
  public user$: Observable<any> = this.privUser.asObservable();

  constructor(private httpClient: HttpClient) { }

  // constructor(private httpClient: HttpClient, persistenceService: PersistenceService) {
  //   persistenceService.defineProperty(this, 'userID', 'myNameProperty');
  // }


  public fetchUser(that: any) {
    // logic that mitch gave for fetching thanks mitch

    this.httpClient.post<User>(environment.apiNpoint + '/FirstServlet/', JSON.stringify(that))
        .subscribe( (payload) => {
          this.privUser.next(payload);
          console.log((payload));
        });
  }

}

