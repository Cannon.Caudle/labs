import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Reimb } from './classes/reimb';
import { User } from './classes/user';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login.service';
import { Subscription } from 'rxjs/Subscription';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ReimbsComponent } from './reimbs/reimbs.component';
import { ReimbService } from './reimb.service';
import { FormsModule } from '@angular/forms';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PersistenceModule } from 'angular-persistence';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ReimbsComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    CurrencyMaskModule,
    PersistenceModule
  ],
  providers: [LoginService, ReimbService],
  bootstrap: [AppComponent]
})
export class AppModule { }
