import { Component, OnInit } from '@angular/core';
import { User } from '../classes/user';
import { LoginService } from '../login.service';
import { Subscription } from 'rxjs/Subscription';
import { HttpClient } from '@angular/common/http';
import { PersistenceService } from 'angular-persistence';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  visible = true;
  userData: User;
  inputA: any;
  inputB: any;
  dataSubscription: Subscription;
  constructor(private http: HttpClient, private loginService: LoginService) { }

  ngOnInit() {
    this.dataSubscription = this.loginService.user$.subscribe((payload) => {
     // console.log(this.userData.userID[0]);
      console.log(payload);
      this.userData = payload;

      if (this.userData = null) {
        this.visible = false;
      }


  });
  }

  getUser(e, ngOnInit) {
    e.preventDefault();
    this.inputA = document.getElementsByName('userName');
    this.inputB = document.getElementsByName('password');
    console.log(this.inputA[0].value);
    console.log(this.inputB[0].value);
    const user: any = {
      userName: this.inputA[0].value,
      password: this.inputB[0].value
    };

    console.log( user);
     this.loginService.fetchUser(user);

  }
}

class Foo {
  constructor(private persistenceService: PersistenceService) {
    persistenceService.set('userName', '');
  }
}
