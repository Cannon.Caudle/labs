import { Component, OnInit } from '@angular/core';
import { Reimb } from '../classes/reimb';
import { ReimbService } from '../reimb.service';
import { Subscription } from 'rxjs/Subscription';
import { HttpClient } from '@angular/common/http';
import { User } from '../classes/user';
import { LoginService } from '../login.service';
// import { userInfo } from 'os';

@Component({
  selector: 'app-reimbs',
  templateUrl: './reimbs.component.html',
  styleUrls: ['./reimbs.component.css']
})
export class ReimbsComponent implements OnInit {
  userData: User;
  inputA: any;
  inputB: any;
  inputC: any;
  reimbs: Array<Reimb> = [];
  dataSubscription: Subscription;
  constructor(private http: HttpClient, private reimbService: ReimbService, private loginService: LoginService) { }
  ngOnInit() {

    this.dataSubscription = this.loginService.user$.subscribe((payload) => {
       this.userData = payload;
      });

    this.reimbService.getReimbs();
    this.dataSubscription = this.reimbService.reimb$.subscribe((payload) => {
      this.reimbs = payload;
    });
      }

    createReimb(e, ngOnInit) {
    e.preventDefault();
    this.inputA = document.getElementsByName('desc');

    this.inputB = document.getElementsByName('amount');
    this.inputC = document.getElementsByName('type');
    const reimb: any = {
      desc: this.inputA[0].value,
      amount: this.inputB[0].value,
      type: this.inputC[0].value,
    };
    console.log(reimb);
    this.reimbService.writeReimb(reimb, this.userData);

  }

}

