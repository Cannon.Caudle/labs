package com.revature.reimbursement.services;

import com.revature.reimbursement.daos.ReimbCreateDaoImp;
import com.revature.reimbursement.daos.ReimbReadDaoImp;
import com.revature.reimbursement.daos.ReimbUpdateDaoImp;
import com.revature.reimbursement.daos.ReimbursementDao;

import java.sql.Timestamp;
import java.util.List;

import com.revature.reimbursement.beans.ReimbBean;
import com.revature.reimbursement.beans.UserBean;
import com.revature.reimbursement.services.Hashing;

public class UserServices {

	private ReimbursementDao daoU = new ReimbUpdateDaoImp();
	private ReimbursementDao daoR = new ReimbReadDaoImp();
	private ReimbursementDao daoC = new ReimbCreateDaoImp();

	public UserBean userLogin(String acctname, String password, UserBean user) {
		password = Hashing.sha256(password);
		return daoR.userLogin(acctname, password, user);
	}
	
	public UserBean createUserAccount(String fname, String lname, String acctname, String password, String eMail,
			int userRole) {
		UserBean user = new UserBean();
		user.setUserName(acctname);
		user.setPassword(Hashing.sha256(password));
		user.setlName(lname);
		user.setfName(fname);
		user.seteMail(eMail);
		user.setUserRole(userRole);
		daoC.saveUser(user);
		return user;
	}
			//		(reimb.getAmount(), reimb.getDesc(), "empty Recipt", 1, reimb.getType(), user);
	public ReimbBean createReimbursment(double amount, String desc, String receipt, int status, int type,
			UserBean user) {
		ReimbBean reimb = new ReimbBean();
		reimb.setAmount(amount);
		reimb.setReceipt(receipt);
		reimb.setDesc(desc);
		
		daoC.saveReimb(user, reimb, status, type);
		return reimb;
	}

	public ReimbBean reimbResolver(ReimbBean reimb, UserBean user, Timestamp currentT, int status) {
		return daoU.reimbResolver(reimb, user, currentT, status);
	}

	public ReimbBean readReimb(int reimbId, UserBean user, ReimbBean reimb) {
		return daoR.readReimb(reimbId, user, reimb);
	}
	public List<ReimbBean> getAllReimbs(){
		return daoR.getAllReimbs();
	}
	
	public List<ReimbBean> getResolvedReimbs(int status){
		return daoR.getResolvedReimbs(status);
	}

}
