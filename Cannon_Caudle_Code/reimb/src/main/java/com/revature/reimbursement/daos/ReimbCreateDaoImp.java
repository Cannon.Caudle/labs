package com.revature.reimbursement.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import com.revature.reimbursement.beans.ReimbBean;
import com.revature.reimbursement.beans.UserBean;
import com.revature.reimbursement.connection.util.ConnectionUtil;

public class ReimbCreateDaoImp implements ReimbursementDao{
	private ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();
	/* Create/save user */
	public UserBean saveUser(UserBean user) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO ERS_USERS (ers_USERNAME, ers_PASSWORD, user_FIRST_NAME, user_LAST_NAME,"
					+ "user_EMAIL, user_ROLE_ID) VALUES (?,?,?,?,?,?) RETURNING ers_USERS_ID";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getUserName());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getfName());
			ps.setString(4, user.getlName());
			ps.setString(5, user.geteMail());
			ps.setInt(6, user.getUserRole());

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				user.setUserID(rs.getInt("ers_USERS_ID"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}
	/*  insert into the little tables to get the id    */
	
	
	
	
	public ReimbBean saveReimb(UserBean user, ReimbBean reimb, int status, int type) {
		Calendar calendar = Calendar.getInstance();
	    java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO ERS_REIMBURSEMENT (reimb_AMOUNT, reimb_DESCRIPTION, reimb_RECEIPT,"
					+ "reimb_AUTHOR, reimb_STATUS_ID, reimb_TYPE_ID, reimb_SUBMITTED) VALUES (?,?,?,?,?,?,?) RETURNING reimb_ID";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setDouble(1, reimb.getAmount());
			ps.setString(2, reimb.getDesc());
			ps.setString(3, reimb.getReceipt());
			// Hardcode the author 
			ps.setInt(4, 1);
			ps.setInt(5, status);
			ps.setInt(6, type);
			ps.setTimestamp(7, ourJavaTimestampObject);
			
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				reimb.setrID(rs.getInt("reimb_ID"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reimb;
	}




	@Override
	public ReimbBean readReimb(int reimbId, UserBean user, ReimbBean reimb) {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public UserBean userLogin(String acctname, String password, UserBean user) {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public ReimbBean reimbResolver(ReimbBean reimb, UserBean user, Timestamp currentT, int status) {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public List<ReimbBean> getAllReimbs() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public List<ReimbBean> getResolvedReimbs(int status) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
