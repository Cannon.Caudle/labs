package com.revature.reimbursement.controllers;
import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.ConsoleHandler;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.catalina.servlets.DefaultServlet;
import org.apache.log4j.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.reimbursement.beans.UserBean;
import com.revature.reimbursement.services.UserServices;


public class FirstServlet extends DefaultServlet{

	UserBean user = new UserBean();
	UserServices service = new UserServices();

	private static final long serialVersionUID = 1L;
	private static final Logger Log = Logger.getRootLogger();
	 
	@Override
	public void init() {
		Log.trace("MyFirstServlet is starting! Login");
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		BufferedReader JSON = request.getReader();
		// Log.trace(JSON);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		ObjectMapper OM = new ObjectMapper();
		UserBean userA = OM.readValue(JSON, UserBean.class);
		
	    String userName = (request.getParameter("userName"));  
	    String password = (request.getParameter("password"));
	    System.out.println(userA.getUserName() + userA.getPassword());
	    if (service.userLogin(userA.getUserName(), userA.getPassword(), user) != null) {
	    	response.getWriter().write(OM.writeValueAsString(user));
	    }
    else {
    	Log.trace("darkness my old friend");
    }
	    out.close();
	}
}
