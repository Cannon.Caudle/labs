package com.revature.reimbursement.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.revature.reimbursement.connection.util.*;
import com.revature.reimbursement.beans.*;

public class ReimbReadDaoImp implements ReimbursementDao {
	private ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();

	/* CRU but prolly not delete CREATE READ UPdate  
	 * Need to add some more imps 
	 * get all reimbs isn't anywhere else need to add that
	 * to the other files
	 * */

	
	private ReimbBean extractReimb(ResultSet rs) throws SQLException {

		ReimbBean reimb = new ReimbBean();
		reimb.setrID(rs.getInt("reimb_ID"));
		reimb.setAmount(rs.getDouble("reimb_AMOUNT"));
		reimb.setDesc(rs.getString("reimb_DESCRIPTION"));
		reimb.setReceipt(rs.getString("reimb_RECEIPT"));
		reimb.setStatus(rs.getString("reimb_STATUS"));
		reimb.setType(rs.getInt("reimb_TYPE_ID"));
		
		return reimb;
	}
	
	/* read user */
	public UserBean userLogin(String acctname, String password, UserBean user) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT * FROM ERS_USERS WHERE ers_USERNAME=? and ers_PASSWORD=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, acctname);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				user.setUserID(rs.getInt("ers_USERS_ID"));
				user.seteMail(rs.getString("user_EMAIL"));
				user.setfName(rs.getString("user_FIRST_NAME"));
				user.setlName(rs.getString("user_LAST_NAME"));
				user.setUserName(rs.getString("ers_USERNAME"));
				user.setPassword(rs.getString("ers_PASSWORD"));
			} else {
				System.out.println("we didnt find that account");
			}
			rs.close();
			ps.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public ReimbBean readReimb(int reimbId, UserBean user, ReimbBean reimb) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT * FROM ADMIN_GET_SPEC_USER WHERE reimb_ID=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reimbId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				reimb = extractReimb(rs);
			} else {
				System.out.println("we didnt find that account");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return reimb;
	}
	
	public List<ReimbBean> getAllReimbs(){
		try(Connection conn = connectionUtil.getConnection()){
			String sql = ("Select * from ADMIN_GET_SPEC_USER");
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<ReimbBean> reimbs = new ArrayList<>();
			while(rs.next()) {
				reimbs.add(extractReimb(rs));
			}
			return reimbs;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<ReimbBean> getResolvedReimbs(int status){
		try(Connection conn = connectionUtil.getConnection()){
			String sql = ("Select * from ADMIN_GET_SPEC_USER WHERE reimb_STATUS_ID=?");
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, status);
			ResultSet rs = ps.executeQuery();
			List<ReimbBean> reimbs = new ArrayList<>();
			while(rs.next()) {
				reimbs.add(extractReimb(rs));
			}
			return reimbs;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	

	@Override
	public UserBean saveUser(UserBean user) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ReimbBean reimbResolver(ReimbBean reimb, UserBean user, Timestamp currentT, int status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReimbBean saveReimb(UserBean user, ReimbBean reimb, int status, int type) {
		// TODO Auto-generated method stub
		return null;
	}

	
	//  ADMIN_GET_SPEC_USER select * Where username=? 
	// will return all reimb tickets for that 

	
}


/* read reimbursement account    
 * I'm not sure about this one 
 * The flaw is it may not show enough 
 * for what the circumstance may be
 * 
 * */
/*public ReimbBean getAcctIDFromUser(ReimbBean reimb, UserBean user) {
	try (Connection conn = connectionUtil.getConnection()) {
		String sql = "SELECT * FROM REIMB_AND_STATUS WHERE reimb_AUTHOR = ERS_USERS.ers_USERNAME AND ERS_USERS.ers_USERNAME =?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, user.getUserName());
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			reimb.setrID(rs.getInt("reimb_ID"));
			reimb.setAmount(rs.getInt("reimb_AMOUNT"));
			reimb.setResolved(rs.getBoolean("reimb_STATUS"));
			reimb.setAmount(rs.getDouble("reimb_AMOUNT"));
			reimb.setDesc(rs.getString("reimb_DESCRIPTION"));

		} else {
			System.out.println("we didnt find that Reimbursement");
		}
		rs.close();
		ps.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return reimb;
}*/
