package com.revature.reimbursement.daos;

import java.sql.Timestamp;
import java.util.List;

import com.revature.reimbursement.beans.ReimbBean;
import com.revature.reimbursement.beans.UserBean;

public  interface ReimbursementDao {

	public UserBean saveUser(UserBean user);
	
	public ReimbBean saveReimb(UserBean user, ReimbBean reimb, int status, int type);
	
	public ReimbBean readReimb(int reimbId, UserBean user, ReimbBean reimb);
	
	public UserBean userLogin(String acctname, String password, UserBean user);
	
	public ReimbBean reimbResolver(ReimbBean reimb, UserBean user,Timestamp currentT ,int status);
	
	public List<ReimbBean> getAllReimbs();
	
	public List<ReimbBean> getResolvedReimbs(int status);
}
