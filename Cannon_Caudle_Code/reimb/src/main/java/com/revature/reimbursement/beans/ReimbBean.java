package com.revature.reimbursement.beans;

public class ReimbBean {
	
	private double amount;
	private String submitted;
	private boolean resolved;
	private String desc;
	private String receipt;
	private int rID;
	private int type;
	private String status;
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getSubmitted() {
		return submitted;
	}
	public void setSubmitted(String submitted) {
		this.submitted = submitted;
	}
	public boolean getResolved() {
		return resolved;
	}
	public void setResolved(boolean b) {
		this.resolved = b;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getReceipt() {
		return receipt;
	}
	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}
	public int getrID() {
		return rID;
	}
	public void setrID(int rID) {
		this.rID = rID;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "ReimbBean [amount=" + amount + ", submitted=" + submitted + ", resolved=" + resolved + ", desc=" + desc
				+ ", receipt=" + receipt + ", rID=" + rID + ", type=" + type + ", status=" + status + "]";
	}
	public ReimbBean(double amount, String submitted, boolean resolved, String desc, String receipt, int rID,
			int type, String status) {
		super();
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.desc = desc;
		this.receipt = receipt;
		this.rID = rID;
		this.type = type;
		this.status = status;
	}
	public ReimbBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
