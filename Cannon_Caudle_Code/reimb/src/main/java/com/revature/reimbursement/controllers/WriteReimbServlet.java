package com.revature.reimbursement.controllers;
import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.ConsoleHandler;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.catalina.servlets.DefaultServlet;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.reimbursement.beans.JacksBean;
import com.revature.reimbursement.beans.ReimbBean;
import com.revature.reimbursement.beans.UserBean;
import com.revature.reimbursement.services.UserServices;


@JsonIgnoreProperties(ignoreUnknown=true)
public class WriteReimbServlet extends DefaultServlet {

	UserBean user = new UserBean();
	ReimbBean reimb = new ReimbBean();
	
	UserServices service = new UserServices();
	
	private static final long serialVersionUID = 1L;
	private static final Logger Log = Logger.getRootLogger();
	
	/**
	 * made to create a riemb using some hard coded values 
	 * user id disappears somewhere between hear and daoimp
	 * status is also hardcoded due to poor bean setup 
	 * use a master bean jack to get the data then from that delegate values out
	 * 
	 * I Realize now i could just put userId in reimb bean and in my reimb class
	 * Would have saved me so much time 
	 */
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		BufferedReader JSON = request.getReader();
		BufferedReader JSON2 = request.getReader();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		ObjectMapper OM = new ObjectMapper();
		ObjectMapper OM2 = new ObjectMapper();
		OM.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		OM2.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		JacksBean jack = OM.readValue(JSON, JacksBean.class);
		
		user.setUserID(jack.getrID());
		user.setPassword(jack.getPassword());
		user.setUserName(jack.getUserName());
		
		reimb.setAmount(jack.getAmount());
		reimb.setDesc(jack.getDesc());
		reimb.setType(jack.getType());
		
//		String desc = (request.getParameter("desc"));  
//	    String amountS = (request.getParameter("amount"));
//	    double amount = Double.valueOf(amountS);
//	    String typeS = (request.getParameter("type"));
//	    int type = Integer.valueOf(typeS);
//		String amountS = new String((request.getParameter("amount")));
//		String amountN = new String(amountS);
//		double amount = Double.parseDouble(amountN);
//		reimb.setAmount(Double.parseDouble(request.getParameter("amount")));
//		reimb.setDesc(request.getParameter("desc"));
//		reimb.setrID(Integer.parseInt(request.getParameter("rID")));
//		reimb.setReceipt(request.getParameter("receipt"));
//		reimb.setResolved(Boolean.parseBoolean(request.getParameter("resolved")));
//		reimb.setStatus(request.getParameter("status"));
//		reimb.setSubmitted(request.getParameter("submitted"));
//		reimb.setType(Integer.parseInt(request.getParameter("type")));
//		
//		
//		
//		String userName = (request.getParameter("userName"));
//		String password = (request.getParameter("password"));
//		String lName = (request.getParameter("lName"));
//		String fName = (request.getParameter("fName"));
//		String eMail = (request.getParameter("eMail"));
//		int userID = Integer.parseInt((request.getParameter("userID")));
//		int userRole = Integer.parseInt((request.getParameter("userRole")));
//		user.setUserID(userID);
//		user.setfName(fName);
//		user.seteMail(eMail);
//		user.setlName(lName);
//		user.setPassword(password);
//		user.setUserName(userName);
//		user.setUserRole(userRole);
		
		System.out.println("HERERERERERERERERERE");
		//System.out.println((jack.toString()));
	    service.createReimbursment(reimb.getAmount(), reimb.getDesc(), "empty Recipt", 1, reimb.getType(), user);
	    response.getWriter().write(OM.writeValueAsString(reimb.getrID()));
	    out.close();
	}
}
