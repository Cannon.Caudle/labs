package com.revature.reimbursement.beans;

public class JacksBean {

	
	private String userName;
	private String password;
	private String lName;
	private String fName;
	private String eMail;
	private int userID;
	private int userRole;
	private double amount;
	private String submitted;
	private boolean resolved;
	private String desc;
	private String receipt;
	private int rID;
	private int type;
	private String status;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String geteMail() {
		return eMail;
	}
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public int getUserRole() {
		return userRole;
	}
	public void setUserRole(int userRole) {
		this.userRole = userRole;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getSubmitted() {
		return submitted;
	}
	public void setSubmitted(String submitted) {
		this.submitted = submitted;
	}
	public boolean isResolved() {
		return resolved;
	}
	public void setResolved(boolean resolved) {
		this.resolved = resolved;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getReceipt() {
		return receipt;
	}
	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}
	public int getrID() {
		return rID;
	}
	public void setrID(int rID) {
		this.rID = rID;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "JacksBean [userName=" + userName + ", password=" + password + ", lName=" + lName + ", fName=" + fName
				+ ", eMail=" + eMail + ", userID=" + userID + ", userRole=" + userRole + ", amount=" + amount
				+ ", submitted=" + submitted + ", resolved=" + resolved + ", desc=" + desc + ", receipt=" + receipt
				+ ", rID=" + rID + ", type=" + type + ", status=" + status + "]";
	}
	public JacksBean(String userName, String password, String lName, String fName, String eMail, int userID,
			int userRole, double amount, String submitted, boolean resolved, String desc, String receipt, int rID,
			int type, String status) {
		super();
		this.userName = userName;
		this.password = password;
		this.lName = lName;
		this.fName = fName;
		this.eMail = eMail;
		this.userID = userID;
		this.userRole = userRole;
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.desc = desc;
		this.receipt = receipt;
		this.rID = rID;
		this.type = type;
		this.status = status;
	}
	public JacksBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
