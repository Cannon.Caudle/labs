package com.revature.reimbursement.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import com.revature.reimbursement.beans.ReimbBean;
import com.revature.reimbursement.beans.UserBean;
import com.revature.reimbursement.connection.util.ConnectionUtil;

public class ReimbUpdateDaoImp implements ReimbursementDao {
	private ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();

	public ReimbBean reimbResolver(ReimbBean reimb, UserBean user,Timestamp currentT ,int status){
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "UPDATE ERS_REIMBURSEMENT SET reimb_Resolver=?, "
					+ "reimb_RESOLVED=?, reimb_STATUS_ID=?  WHERE reimb_ID=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, user.getUserID());
			ps.setTimestamp(2, currentT);
			ps.setInt(3, status);
			ps.setInt(4, reimb.getrID());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return reimb;
	}

	@Override
	public UserBean saveUser(UserBean user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReimbBean readReimb(int reimbId, UserBean user, ReimbBean reimb) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserBean userLogin(String acctname, String password, UserBean user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ReimbBean> getAllReimbs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReimbBean saveReimb(UserBean user, ReimbBean reimb, int status, int type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ReimbBean> getResolvedReimbs(int status) {
		// TODO Auto-generated method stub
		return null;
	}

}
