package com.revature.reimbursement.controllers;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.ConsoleHandler;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.catalina.servlets.DefaultServlet;
import org.apache.log4j.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.reimbursement.beans.UserBean;
import com.revature.reimbursement.beans.ReimbBean;
import com.revature.reimbursement.services.UserServices;

public class ReimbsGrabber extends DefaultServlet{

	UserBean user = new UserBean();
	ReimbBean reimb = new ReimbBean();
	UserServices service = new UserServices();

	private static final long serialVersionUID = 1L;
	private static final Logger Log = Logger.getRootLogger();
	 
	
	/**
	 * returns all the reimbursments
	 */
	
	@Override
	public void init() {
		Log.trace("MyFirstServlet is starting! Login");
	}
	
	@Override
	 protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		BufferedReader JSON = request.getReader();
		Log.trace(JSON);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		ObjectMapper OM = new ObjectMapper();
		//ReimbBean reimb = OM.readValue(JSON, ReimbBean.class);
		  
		System.out.println("----------------Reimbsgrabber");
	    
		 response.getWriter().write(OM.writeValueAsString(service.getAllReimbs()));
	    	
	    out.close();
	}
	
}
